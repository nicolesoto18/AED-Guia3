#include <iostream>
#include "Lista.h"

using namespace std;

int main(){
    // Crear lista
    Lista *lista =  new Lista();
    int salir = 1;
    int numero;

    cout << "\n\tLISTA ENLAZADA" << endl;

    while(salir == 1){
        cout << "Ingrese un número: ";
        cin >> numero;
        
        system("clear");

        // Se crea la lista y se agregan los elementos
        lista->crear_nodo(numero);

        //lista->ordenar_lista();
        lista->imprimir();

        cout << "\n\n [1] Seguir ingresando números\n [2] Salir" << endl;
        cin >> salir;
    }

    return 0;
}
