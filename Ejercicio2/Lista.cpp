#include <iostream>

//Definición de la clase
#include "Lista.h"

using namespace std;

Lista::Lista(){

}

void Lista::crear_nodo(int num){
    Nodo *temporal = new Nodo;

    // Se liga al nodo siguiente, creando el espacio de memoria
    temporal->numero = num; 
    temporal->nodo_sig = NULL;

    // Si el es primer nodo de la lista, lo deja como raíz y como último nodo
    if (this->inicio == NULL) { 
        this->inicio = temporal;
        this->ultimo = this->inicio;
    } 

    // Si no apunta el actual último nodo al nuevo y deja el nuevo como el último de la lista
    else {
        this->ultimo->nodo_sig = temporal;
        this->ultimo = temporal;
    }

    ordenar_lista(this->inicio);
}
 
void Lista::ordenar_lista(Nodo *temporal){
    int aux;

    while(temporal != NULL){
        Nodo *temporal2 = temporal->nodo_sig;
            while(temporal2 != NULL){
                if(temporal->numero > temporal2->numero){
                    aux = temporal2->numero;
                    temporal2->numero = temporal->numero;
                    temporal->numero = aux;
                }
            temporal2 = temporal2->nodo_sig;
        }
    temporal = temporal->nodo_sig;
    }

}

void Lista::imprimir(){
    Nodo *temporal = this->inicio;
    
    while (temporal != NULL) {
        cout << temporal->numero << " - ";
        temporal = temporal->nodo_sig;
    }
    
    cout << "\n";

}

Nodo* Lista::get_inicio(){
    return this->inicio;
}


