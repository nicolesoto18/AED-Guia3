#include <iostream>
#include "Lista.h"

using namespace std;

void pedir_numeros(Lista *lista){
    int numero;
    int salir;

    while(salir != 2){
    cout << "Ingrese un número: ";
    cin >> numero;
             
    // Se crea la lista y se agregan los elementos
    lista->crear_nodo(numero);

    cout << " [1] Seguir ingresando números\n [2] Salir" << endl;
    cin >> salir;
    }
}
    
void combinar_listas(Lista *lista, Lista *lista3){
    int num;
    // Utiliza el puntero que apunta al inicio de la lista
    Nodo *temporal = lista->get_inicio();
    
    while(temporal != NULL){
        // Agrega el número de las listas ya creadas a una tercera
        num = temporal->numero;
        lista3->crear_nodo(num);

        // Apunta al nodo siguiente
        temporal = temporal->nodo_sig;
    }
}

void menu(Lista *lista1, Lista *lista2, Lista *lista3){
    int opcion;
    int ingresar;
 
    cout << "\n\nMENU\n" << endl;
    cout << " [1] Agregar números\n [2] Ver listas\n [3] Salir" << endl;
     
    cout << "\n Ingrese su opción: ";
    cin >> opcion;
    
    system("clear");

    switch(opcion){
        case 1:
            cout << "\n [1] Agregar números a la lista 1 \n [2] Agregar números a la lista 2" << endl;
            cin >> ingresar;

            if (ingresar == 1){
                pedir_numeros(lista1);
                combinar_listas(lista1, lista3);
            }
            
            else if (ingresar == 2){
                pedir_numeros(lista2);
                combinar_listas(lista2, lista3);
            }
            
            menu(lista1, lista2, lista3);
            break;


        case 2:
            cout << "\tLISTA ORDENADA 1" << endl;
            lista1->imprimir();

            cout << "\tLISTA ORDENADA 2" << endl;
            lista2->imprimir();
            
            cout << "\tLISTA ORDENADA 3" << endl;
            lista3->imprimir();
            menu(lista1, lista2, lista3);

            break;

        case 3:
            break;
    }
}



int main(){
    // Crear lista
    Lista *lista1 =  new Lista();
    Lista *lista2 =  new Lista();
    Lista *lista3 =  new Lista();

    cout << "\n\tLISTA ENLAZADA COMBINADAS";
    menu(lista1, lista2, lista3);

    return 0;
}
