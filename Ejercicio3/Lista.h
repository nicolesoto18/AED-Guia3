#include <iostream>

// Estructura de nodo
#include "programa.h"

using namespace std;

#ifndef LISTA_H
#define LISTA_H

class Lista{
    private:
        Nodo *inicio = 0;
        Nodo *ultimo = 0;

    public:
        Lista();
        void crear_nodo(int num);
        void ordenar_lista(Nodo *temporal);
        void imprimir();
        Nodo* get_inicio();
};
#endif
        
