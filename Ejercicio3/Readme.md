						    
                                                    LISTAS ENLAZADAS        
							                    ------------------------

+ Empezando
    El programa cosiste en la creación de una lista enlazada, inicialmente se pedira agregar un número, este se solicitara hasta que eliga la opción 2 la cual le permite salir del ingreso de números, cuando salga del ingreso de números se mostrara la lista rellenada desde el número menor al mayor.
    Ejemplo:
        Lista ingresada --> 1 - 7 - 8
        Lista a mostar  --> 1 - 2 - 3 - 4 - 5 - 6 - 7 - 8 


+ Ejecutando las pruebas por terminal
    Para la entrada a los archivos en el editor de texto vim, se necesita del comando: 
    [1] vim nombrearchivo.h para archivos .h donde se encuemtra la definición de la clase.
	[2] vim nombrearchivo.cpp para archivos .cpp donde esta la implementación de la clase.
	
    Para compilar el programa se utiliza el comando:
	[3] make

    Mientras que para ejecutar debemos colocar:
    [4] ./programa
		        

+ Construido con
    Sistema operativo: Ubuntu 18.04.3 LTS.
    Vim: Editor de texto para escribir el código del programa. 
    C++: Lenguaje de programación imperativo orientado a objetos derivado del lenguaje C.
    Pep-8: La narración del código realizado en este proyecto esta basado en las instrucciones dadas por la pep-8.

+ Versiones
    Ubuntu 18.04.3 LTS.
    c++ (Ubuntu 7.4.0-1ubuntu1~18.04.1) 7.4.0

+ Autor
    Nicole Soto.




