#include <iostream>
#include "Lista.h"

using namespace std;

void completar_lista(Lista *lista){
    Nodo *temporal = lista->get_inicio();
    Nodo *temporal2;
    int num;
    
    temporal2 = temporal->nodo_sig;

    system("clear");    
    cout << "\tLISTA RELLENADA\n" << endl;

    while(temporal2 != NULL){
        // Se agregara el número consecutivo
        if(temporal2->numero != temporal->numero + 1){
            num = temporal->numero + 1;
            lista->crear_nodo(num);                    
            lista->imprimir();
        }

    // Se mueve al nodo siguiente
    temporal = temporal2;
    temporal2 = temporal2->nodo_sig;

    }
}

int main(){
    // Crear lista
    Lista *lista =  new Lista();
    int salir = 1;
    int numero;

    cout << "\n\tLISTA ENLAZADA" << endl;

    while(salir == 1){
        cout << "Ingrese un número: ";
        cin >> numero;
        
        system("clear");

        // Se crea la lista y se agregan los elementos
        lista->crear_nodo(numero);

        // Imprime la lista ingresada por el usuario
        lista->imprimir();

        cout << "\n\n [1] Seguir ingresando números\n [2] Salir" << endl;
        cin >> salir;
    }
    
    // Rellena la lista hasta el mayor número ingresado
    completar_lista(lista);

    return 0;
}
